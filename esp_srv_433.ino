/*
  ESP32 433.92MHz nor-tec data logger and server

  Copyright 2020 Hagen Patzke <hpatzke@gmx.net>

  Environment: NodeMCU-32S, COM3, 115200


*/
#define OPT_WIFI 0

#if OPT_WIFI
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiClient.h>

#define CRED_SSID "wifi_ssid"
#define CRED_PASS "wifi-pass"

const char* ssid = CRED_SSID;
const char* pass = CRED_PASS;
#endif // OPT_WIFI

//// NORTEC DECODER

// RX module for 433.92MHz: DATA=D26 (NodeMCU-32S)
#define DATAPIN      26
#define LOG_SIZE     20

// Nortec datagram is 5 bytes long
#define LEN_DATAGRAM_BITS  40
#define LEN_DATAGRAM_BYTES 5
#define LEVEL_PRE    1     // level of the fixed width prefix impulse
#define LEN_PRE_MIN  400
#define LEN_PRE_MAX  600
#define LEN_BI0_MIN  1250
#define LEN_BI1_MIN  3250
#define LEN_STA_MIN  7000
#define LEN_END_MIN  10000

// ring buffer for received data
byte ring_buffer[LOG_SIZE * LEN_DATAGRAM_BYTES];
unsigned long deltaTime[LOG_SIZE]; // used for printing delta time (serial only)
bool received = false; // main loop shortcut if we have no data
int posLastReceived = 0;
int posCurrReceiving = 1;
int posFirstReceived = 1;
int posOurLast = 1;
int loopCount = 0;
int cntSkip = 0;

#define MAXLEN 60
char line[MAXLEN];  // output buffer
byte lofs = 0;      // line offset
char filename[14] = "datalog.txt";  // file name 8+1+3+nul+safety

struct MODRX433 {
  const uint8_t PIN;
  uint32_t numberSignalChanges;
  bool changed;
};

MODRX433 mod_rx {DATAPIN, 0, false};

void IRAM_ATTR handle_rx() {
  static unsigned long tsLast = 0;
  static unsigned long timeLast = 0;
  static long durationLast = 0;
  static byte levelLast = 0;
  static byte posCurBit = 0;

  unsigned long timeNow  = micros();
  byte levelNow = 1 - (digitalRead(DATAPIN) & 1); // level of currently COMPLETED gap/signal
  long durationNow = timeNow - timeLast;

  if ((levelLast == LEVEL_PRE) && (durationLast > LEN_PRE_MIN) && (durationLast < LEN_PRE_MAX)) {
    // we get an interrupt when the signal changes, so now we are processing a signal duration
    // to save computation time, we only check minimal lengths
    // and now we just got a signal pulse in - max 500us time to process stuff
    if (durationNow > LEN_END_MIN) { // end-of-datagram gap
      posCurBit = 0; // reset bit position within datagram
    } else if (durationNow > LEN_STA_MIN) { // start-of-datagram gap
      posCurBit = 0;
      deltaTime[posCurrReceiving] = timeNow - tsLast;
      tsLast = timeNow;
    } else { // we probably have a data bit
      byte  d = (posCurBit % LEN_DATAGRAM_BITS) >> 3; // make sure we stay within our datagram
      byte *p = ring_buffer + (posCurrReceiving * LEN_DATAGRAM_BYTES) + d;
      byte  m = 0x80 >> (posCurBit & 7); // bitmask
      if (durationNow > LEN_BI1_MIN) { // it is a one
        *p |= m;
        posCurBit++;
      } else if (durationNow > LEN_BI0_MIN) { // it is a zero
        *p &= ~m;
        posCurBit++;
      } else { // error: the pulse we got is too short - lets reset the datagram
        posCurBit = 0;
      }
      if (posCurBit == LEN_DATAGRAM_BITS) { // we have all bits
        posLastReceived = posCurrReceiving;
        posCurrReceiving = (posCurrReceiving + 1) % LOG_SIZE;
        if (!received) { // no output running, log start pos of first received datagram
          posFirstReceived = posLastReceived;
          received = true;
        }
      }
    } // data bit
  }

  timeLast = timeNow;
  levelLast = levelNow;
  durationLast = durationNow;
} // end handle_rx

bool nortec_crc_ok(byte *rcvPtr) {
  byte m[LEN_DATAGRAM_BYTES];
  memcpy(m, rcvPtr, LEN_DATAGRAM_BYTES);
  byte msg_crc = m[1] >> 4;
  m[1] = (m[4] << 4) | (m[1] & 0x0F); // for computation, channel info (last nibble) is at CRC position
  byte new_crc = m[0] >> 4; // CRC preload = 0, therefore we can skip these 4 bits
  for (int k = 4; k < 36; k++) {
    new_crc <<= 1;
    if (m[k >> 3] & (0x80 >> (k & 7))) {
      new_crc |= 1;
    }
    if (new_crc & 0x10) {
      new_crc ^= 0x13;
    }
  }
  return (new_crc == msg_crc);
}

// Hex print raw data into buffer
int sprintHex(char line[], byte o, byte *rcvPtr) {
  byte c, n, k;
  for (k = 0; k < LEN_DATAGRAM_BYTES; k++) {
    c = rcvPtr[k];
    n = c >> 4;
    line[o++] = n > 9 ? 55 + n : 48 + n;
    n = c & 0x0f;
    line[o++] = n > 9 ? 55 + n : 48 + n;
  }
  return o;
}

#if OPT_WIFI
// TCP server at port 80 will respond to HTTP requests
WiFiServer HTTP_Server(80);
#endif // OPT_WIFI


void setup(void)
{
  Serial.begin(115200);

#if OPT_WIFI
  // Connect to WiFi network
  WiFi.begin(ssid, pass);
  Serial.println("");

  // Wait for connection
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("ok");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // Set up mDNS responder:
  // - first argument is the domain name, in this example
  //   the fully-qualified domain name is "esp32.local"
  // - second argument is the IP address to advertise
  //   we send our IP address on the WiFi network
  if (!MDNS.begin("esp32")) {
    Serial.println("Error setting up MDNS responder!");
    while (1) {
      delay(1000);
    }
  }
  Serial.println("mDNS responder started");

  // Start TCP (HTTP) server
  HTTP_Server.begin();
  Serial.println("HTTP server started");

  // Add service to MDNS-SD
  MDNS.addService("http", "tcp", 80);
#endif // OPT_WIFI


  Serial.println("\r\nnor-tec receiver");
  // 433.920MHz receiver module
  pinMode(mod_rx.PIN, INPUT_PULLUP);
  attachInterrupt(mod_rx.PIN, handle_rx, CHANGE);
}

unsigned long running_delta = 0;

void loop(void)
{
#if 0
  if (received) {
    byte *rec = ring_buffer;
    for (int i = 0; i < LOG_SIZE; i++) {
      lofs = sprintHex(line, 0, rec);
      line[lofs] = '\0';
      float dt = deltaTime[i] / 1000;
      Serial.print(char(i + 65));
      Serial.print(",");
      Serial.print(line);
      Serial.print(",");
      Serial.print(nortec_crc_ok(rec));
      Serial.print(",");
      Serial.print(dt);
      if (i == posFirstReceived) {
        Serial.print(",F");
      } else if (i == posCurrReceiving) {
        Serial.print(",C");
      }
      Serial.println();
      rec += LEN_DATAGRAM_BYTES;
    }
    Serial.println();
    received = false;
  }
#else
  while (posFirstReceived != posCurrReceiving) {
    byte *rec = &ring_buffer[posFirstReceived * LEN_DATAGRAM_BYTES];
    bool crc_ok = nortec_crc_ok(rec);
    running_delta += deltaTime[posFirstReceived];
    if (crc_ok) {
      if (running_delta > 1200000) {
        lofs = 0;
        lofs = sprintHex(line, lofs, rec);
        line[lofs] = '\0';
        float dt = deltaTime[posFirstReceived] / 1000;
        Serial.print(line);
        Serial.print(",");
        Serial.print(crc_ok);
        Serial.print(",");
        Serial.print(running_delta);
        Serial.print(",");
        Serial.println(dt);
        running_delta = 0;
      }
    }
    posFirstReceived = (posFirstReceived + 1) % LOG_SIZE;
  }
#endif

#if OPT_WIFI
  // Check if a client has connected
  WiFiClient client = HTTP_Server.available();
  if (!client) {
    return;
  }
  Serial.println("");
  Serial.println("New client");

  // Wait for data from client to become available
  while (client.connected() && !client.available()) {
    delay(1);
  }

  // Read the first line of HTTP request
  String req = client.readStringUntil('\r');

  // First line of HTTP request looks like "GET /path HTTP/1.1"
  // Retrieve the "/path" part by finding the spaces
  int addr_start = req.indexOf(' ');
  int addr_end = req.indexOf(' ', addr_start + 1);
  if (addr_start == -1 || addr_end == -1) {
    Serial.print("Invalid request: ");
    Serial.println(req);
    return;
  }
  req = req.substring(addr_start + 1, addr_end);
  Serial.print("Request: ");
  Serial.println(req);

  String s;
  if (req == "/")
  {
    IPAddress ip = WiFi.localIP();
    String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
    s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>Hello from ESP32 at ";
    s += ipStr;
    s += "</html>\r\n\r\n";
    Serial.println("Sending 200");
  }
  else
  {
    s = "HTTP/1.1 404 Not Found\r\n\r\n";
    Serial.println("Sending 404");
  }
  client.print(s);

  client.stop();
  Serial.println("Done with client");
#endif // OPT_WIFI

}
